<?php
require_once __DIR__ .'/bootstrap.php';
require_once __DIR__ .'/tests/config.inc';

class GeneralTests extends TestSuite {
	
	private $path;
	
    public function __construct() {
    	parent::__construct();
        $this->TestSuite(get_class($this));
        $this->path = dirname(__FILE__).DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR;
        
        //$this->addFile($this->path.'dummy_test.php');
        //$this->addFile($this->path.'web_test.php');
        $this->collect( $this->path, new SimplePatternCollector('/_test.php/')  );
    }
}

(new GeneralTests())->run();