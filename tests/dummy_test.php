<?php 
use VWIT\TestDummy\Factory;


class DummyTest extends UnitTestCase {



	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}

	public function __destruct(){

	}

	/**
	 * Called before every test
	 */
	public function setup(){

	}

	/**
	 * Called after every test
	 */
	public function teardown(){

	}
	
	public function testConfig(){
		Out::print_line( __FUNCTION__ );
		Out::dump($_ENV, 'Environment');
	
	}

	public function testAlbumCreation(){
		Out::print_line( __FUNCTION__ );
		
		$album = Factory::create('Mock\App\Album');
		Out::dump($album, 'The Album object');
		
	}
	
}



?>