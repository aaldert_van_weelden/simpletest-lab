<?php
require_once __DIR__ .'/../bootstrap.php';
require_once 'resources/mock.class.php';

class ParisTest extends UnitTestCase {

    const ALTERNATE = 'alternate';
    
    public function __construct(){
    	parent::__construct();
    	Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
    }
    
    public function __destruct(){
    
    }

    public function setUp() {
        // Set up the dummy database connection
        ORM::set_db(new MockPDO('sqlite::memory:'));

        // Enable logging
        ORM::configure('logging', true);
    }

    public function tearDown() {
        ORM::configure('logging', false);
        ORM::set_db(null);
    }

    public function testSimpleAutoTableName() {
    	Out::print_line(  __FUNCTION__ );
    	Model::factory('Simple')->find_many();
        $expected = 'SELECT * FROM `simple`';
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testComplexModelClassName() {
    	Out::print_line(  __FUNCTION__ );
        Model::factory('ComplexModelClassName')->find_many();
        $expected = 'SELECT * FROM `complex_model_class_name`';
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testModelWithCustomTable() {
    	Out::print_line(  __FUNCTION__ );
        Model::factory('ModelWithCustomTable')->find_many();
        $expected = 'SELECT * FROM `custom_table`';
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testCustomIDColumn() {
    	Out::print_line(  __FUNCTION__ );
        Model::factory('ModelWithCustomTableAndCustomIdColumn')->find_one(5);
        $expected = "SELECT * FROM `custom_table` WHERE `custom_id_column` = '5' LIMIT 1";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testFilterWithNoArguments() {
    	Out::print_line(  __FUNCTION__ );
        Model::factory('ModelWithFilters')->filter('name_is_fred')->find_many();
        $expected = "SELECT * FROM `model_with_filters` WHERE `name` = 'Fred'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testFilterWithArguments() {
    	Out::print_line(  __FUNCTION__ );
        Model::factory('ModelWithFilters')->filter('name_is', 'Bob')->find_many();
        $expected = "SELECT * FROM `model_with_filters` WHERE `name` = 'Bob'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testInsertData() {
    	Out::print_line(  __FUNCTION__ );
        $widget = Model::factory('Simple')->create();
        $widget->name = "Fred";
        $widget->age = 10;
        $widget->save();
        $expected = "INSERT INTO `simple` (`name`, `age`) VALUES ('Fred', '10')";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testUpdateData() {
    	Out::print_line(  __FUNCTION__ );
        $widget = Model::factory('Simple')->find_one(1);
        $widget->name = "Fred";
        $widget->age = 10;
        $widget->save();
        $expected = "UPDATE `simple` SET `name` = 'Fred', `age` = '10' WHERE `id` = '1'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testDeleteData() {
    	Out::print_line(  __FUNCTION__ );
        $widget = Model::factory('Simple')->find_one(1);
        $widget->delete();
        $expected = "DELETE FROM `simple` WHERE `id` = '1'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testInsertingDataContainingAnExpression() {
    	Out::print_line(  __FUNCTION__ );
        $widget = Model::factory('Simple')->create();
        $widget->name = "Fred";
        $widget->age = 10;
        $widget->set_expr('added', 'NOW()');
        $widget->save();
        $expected = "INSERT INTO `simple` (`name`, `age`, `added`) VALUES ('Fred', '10', NOW())";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testHasOneRelation() {
    	Out::print_line(  __FUNCTION__ );
        $user = Model::factory('UserOne')->find_one(1);
        $profile = $user->profile()->find_one();
        $expected = "SELECT * FROM `profile` WHERE `user_one_id` = '1' LIMIT 1";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testHasOneWithCustomForeignKeyName() {
    	Out::print_line(  __FUNCTION__ );
        $user2 = Model::factory('UserTwo')->find_one(1);
        $profile = $user2->profile()->find_one();
        $expected = "SELECT * FROM `profile` WHERE `my_custom_fk_column` = '1' LIMIT 1";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testBelongsToRelation() {
    	Out::print_line(  __FUNCTION__ );
        $user2 = Model::factory('UserTwo')->find_one(1);
        $profile = $user2->profile()->find_one();
        $profile->user_one_id = 1;
        $user3 = $profile->user()->find_one();
        $expected = "SELECT * FROM `user_one` WHERE `id` = '1' LIMIT 1";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testBelongsToRelationWithCustomForeignKeyName() {
    	Out::print_line(  __FUNCTION__ );
        $profile2 = Model::factory('ProfileTwo')->find_one(1);
        $profile2->custom_user_fk_column = 5;
        $user4 = $profile2->user()->find_one();
        $expected = "SELECT * FROM `user_one` WHERE `id` = '5' LIMIT 1";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testHasManyRelation() {
    	Out::print_line(  __FUNCTION__ );
        $user4 = Model::factory('UserThree')->find_one(1);
        $posts = $user4->posts()->find_many();
        $expected = "SELECT * FROM `post` WHERE `user_three_id` = '1'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testHasManyRelationWithCustomForeignKeyName() {
    	Out::print_line(  __FUNCTION__ );
        $user5 = Model::factory('UserFour')->find_one(1);
        $posts = $user5->posts()->find_many();
        $expected = "SELECT * FROM `post` WHERE `my_custom_fk_column` = '1'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testHasManyThroughRelation() {
    	Out::print_line(  __FUNCTION__ );
        $book = Model::factory('Book')->find_one(1);
        $authors = $book->authors()->find_many();
        $expected = "SELECT `author`.* FROM `author` JOIN `author_book` ON `author`.`id` = `author_book`.`author_id` WHERE `author_book`.`book_id` = '1'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

    public function testHasManyThroughRelationWithCustomIntermediateModelAndKeyNames() {
    	Out::print_line(  __FUNCTION__ );
        $book2 = Model::factory('BookTwo')->find_one(1);
        $authors2 = $book2->authors()->find_many();
        $expected = "SELECT `author`.* FROM `author` JOIN `author_book` ON `author`.`id` = `author_book`.`custom_author_id` WHERE `author_book`.`custom_book_id` = '1'";
        $this->assertEqual($expected, ORM::get_last_query());
    }

}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new ParisTest())->run();