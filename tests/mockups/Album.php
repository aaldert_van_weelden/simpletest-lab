<?php namespace Mock\App;

/**
 * Extend this class from the application model
 * @author Aaldert
 *
 */
class Album{
	
	private $attributes;//remove this  in real life
	private $dto;
	
	public function fill($attributes){
		//instantiate and populate the model dto
		//$this->dto = new DTO($attributes);
		
		$this->attributes = (object) $attributes;
		return $this;
	}
	
	public function getAttributes(){
		return $this->attributes;
	}
	
	public function save(){
		//persist to the (in memory) database 
		//return parent::save($dto);
		
		return $this->attributes;//remove this in real life
	}
}