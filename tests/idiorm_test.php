<?php
require_once __DIR__ .'/../bootstrap.php';
require_once 'resources/mock.class.php';


class IdiormTest extends UnitTestCase {

    const ALTERNATE = 'alternate';
    
    public function __construct(){
    	parent::__construct();
    	Out::print_line(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this)) ;
    }
    
    public function __destruct(){
    
    }

	public function setUp() {
    	// Enable logging
    	ORM::configure('logging', true);
    
    	// Set up the dummy database connection
    	$db = new MockPDO('sqlite::memory:');
    	ORM::set_db($db);
    }
    
    public function tearDown() {
    	ORM::configure('logging', false);
    	ORM::set_db(null);
    }
   
  
    
    	public function testStaticAtrributes() {
    		Out::print_line(  __FUNCTION__ );
    		$this->assertEqual('0', ORM::CONDITION_FRAGMENT);
    		$this->assertEqual('1', ORM::CONDITION_VALUES);
    	}
    
    	public function testForTable() {
    		Out::print_line(  __FUNCTION__ );
    		$result = ORM::for_table('test');
    		$this->assertTrue($result instanceof ORM);
    	}
    
    	public function testCreate() {
    		Out::print_line(  __FUNCTION__ );
    		$model = ORM::for_table('test')->create();
    		$this->assertTrue($model instanceof ORM);
    		$this->assertTrue($model->is_new());
    	}
    
    	public function testIsNew() {
    		Out::print_line(  __FUNCTION__ );
    		$model = ORM::for_table('test')->create();
    		$this->assertTrue($model->is_new());
    
    		$model = ORM::for_table('test')->create(array('test' => 'test'));
    		$this->assertTrue($model->is_new());
    	}
    
    	public function testIsDirty() {
    		Out::print_line(  __FUNCTION__ );
    		$model = ORM::for_table('test')->create();
    		$this->assertFalse($model->is_dirty('test'));
    
    		$model = ORM::for_table('test')->create(array('test' => 'test'));
    		$this->assertTrue($model->is_dirty('test'));
    	}
    
    	public function testArrayAccess() {
    		Out::print_line(  __FUNCTION__ );
    		$value = 'test';
    		$model = ORM::for_table('test')->create();
    		$model['test'] = $value;
    		$this->assertTrue(isset($model['test']));
    		$this->assertEqual($model['test'], $value);
    		unset($model['test']);
    		$this->assertFalse(isset($model['test']));
    	}
    
    	public function testFindResultSet() {
    		Out::print_line(  __FUNCTION__ );
    		$result_set = ORM::for_table('test')->find_result_set();
    		$this->assertTrue($result_set instanceof IdiormResultSet);
    		$this->assertIdentical(count($result_set), 5);
    	}
    
    	public function testFindResultSetByDefault() {
    		Out::print_line(  __FUNCTION__ );
    		ORM::configure('return_result_sets', true);
    
    		$result_set = ORM::for_table('test')->find_many();
    		$this->assertTrue($result_set instanceof IdiormResultSet);
    		$this->assertIdentical(count($result_set), 5);
    
    		ORM::configure('return_result_sets', false);
    
    		$result_set = ORM::for_table('test')->find_many();
    		$this->assertEqual('array', gettype($result_set));
    		$this->assertIdentical(count($result_set), 5);
    	}
    
    	public function testGetLastPdoStatement() {
    		Out::print_line(  __FUNCTION__ );
    		ORM::for_table('widget')->where('name', 'Fred')->find_one();
    		$statement = ORM::get_last_statement();
    		$this->assertTrue( $statement instanceof MockPDOStatement);
    	}
    
  
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new IdiormTest())->run();