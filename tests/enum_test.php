<?php
require_once __DIR__ .'/../bootstrap.php';
/**
 * Put the ENUM class  tests here
 * 
 * @author Aaldert van Weelden
 *
 */
class EnumWithDefaultValue extends Enum
{
	const ONE = 1;
	const TWO = 2;
	public $value = 1;
}

class EnumWithNullAsDefaultValue extends Enum
{
	const NONE = null;
	const ONE  = 1;
	const TWO  = 2;
}


class EnumWithoutDefaultValue extends Enum
{
	const ONE = 1;
	const TWO = 2;
}

class EnumInheritance extends EnumWithoutDefaultValue
{
	const INHERITACE = 'Inheritance';
}

class EmptyEnum extends Enum
{}

/**
 * Tests start here
 * @author Aaldert van Weelden
 *
 */

class EnumTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		
	}
	
	public function testEnumWithDefaultValue(){
		Out::print_line( __FUNCTION__ );
		
		$enum = new EnumWithDefaultValue();
	
		$this->assertEqual(array(
				'ONE' => 1,
				'TWO' => 2,
		), (array)$enum->getConstants());
	
		$this->assertEqual(1, $enum->getValue());
		$this->assertEqual(1, $enum->__invoke());
	
		$this->assertEqual('ONE', $enum->getName());
		$this->assertEqual('ONE', $enum->__toString());
	}
	
	public function testEnumWithNullAsDefaultValue(){
		Out::print_line( __FUNCTION__ );
		
		$enum = new EnumWithNullAsDefaultValue();
	
		$this->assertEqual(array(
				'NONE' => null,
				'ONE'  => 1,
				'TWO'  => 2,
		), (array)$enum->getConstants());
	
		$this->assertNull($enum->getValue());
		$this->assertNull($enum->__invoke());
	
		$this->assertEqual('NONE', $enum->getName());
		$this->assertEqual('NONE', $enum->__toString());
	}
	
	public function testEnumWithoutDefaultValue(){
		Out::print_line( __FUNCTION__ );
		
		//@expected InvalidArgumentException
		try{
			new EnumWithoutDefaultValue();
		}catch(InvalidArgumentException $e){
			Out::print_line( "Expected exception : ".$e->getMessage() );
			$this->pass();
		}
		
	}
	
	public function testEnumInheritance(){
		Out::print_line( __FUNCTION__ );
		
		$enum = new EnumInheritance(EnumInheritance::ONE);
		
		try{
		$this->assertEqual(EnumInheritance::ONE, $enum->getValue());
		}catch(InvalidArgumentException $e){
			Out::print_line( "Expected exception : ".$e->getMessage() );
			$this->pass();
		}
	
		
	}
	
	public function testChangeValueOnConstructor(){
		Out::print_line( __FUNCTION__ );
		
		$enum = new EnumWithoutDefaultValue(1);
	
		$this->assertEqual(1, $enum->getValue());
		$this->assertEqual(1, $enum->__invoke());
	
		$this->assertEqual('ONE', $enum->getName());
		$this->assertEqual('ONE', $enum->__toString());
	}
	
	public function testChangeValueOnConstructorThrowsInvalidArgumentExceptionOnStrictComparison(){
		Out::print_line( __FUNCTION__ );
		
		//@expected InvalidArgumentException
		try{
			$enum = new EnumWithoutDefaultValue('1');
		}catch(InvalidArgumentException $e){
			Out::print_line("Expected exception : ".$e->getMessage());
			$this->pass();
		}
	}
	
	public function testSetValue(){
		Out::print_line( __FUNCTION__ );
		
		$enum = new EnumWithDefaultValue();
		$enum->setValue(2);
	
		$this->assertEqual(2, $enum->getValue());
		$this->assertEqual(2, $enum->__invoke());
	
		$this->assertEqual('TWO', $enum->getName());
		$this->assertEqual('TWO', $enum->__toString());
	}
	
	public function testSetValueThrowsInvalidArgumentExceptionOnStrictComparison(){
		Out::print_line( __FUNCTION__ );
		
		//@expected InvalidArgumentException
		try{
			$enum = new EnumWithDefaultValue();
			$enum->setValue('2');
		}catch(InvalidArgumentException $e){
			Out::print_line("Expected exception : ".$e->getMessage());
			$this->pass();
		}
	}
	
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new EnumTest())->run();

?>